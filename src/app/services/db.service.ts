import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DbService {

  url: string = environment.apiUrl;


  constructor(public http: HttpClient) {

  }

  getAllNodes(topologyID: number) {
    return this.http.get(this.url + '/nodes/all/' + topologyID);
  }

  getNodeByID(nodeID: number) {
    return this.http.get(this.url + '/nodes/' + nodeID);
  }

  getAllEdges(topologyID: number) {
    return this.http.get(this.url + '/edges/all/' + topologyID);
  }

  getEdgeByID(edgeID: number) {
    return this.http.get(this.url + '/edges/all/' + edgeID);
  }

  getAllRuns() {
    return this.http.get(this.url + '/runs/all/');
  }

  getRunByID(runID: number) {
    return this.http.get(this.url + '/runs/' + runID);
  }

  getAllTopologies(runID: number) {
    return this.http.get(this.url + '/topologies/all/' + runID);
  }

  getTopologyByID(topologyID: number) {
    return this.http.get(this.url + '/topologies/' + topologyID);
  }

  getAllMessages(topologyID: number) {
    return this.http.get(this.url + '/messages/get/all/' + topologyID);
  }

  getMessageByID(messageID: number) {
    return this.http.get(this.url + '/messages/get/' + messageID);
  }

  getAllPaths(messageID: number) {
    return this.http.get(this.url + '/path/get/all/' + messageID);
  }

  getPathByID(pathID: number) {
    return this.http.get(this.url + '/path/get/' + pathID);
  }

  getMovieByID(runID: number) {
    return this.http.get(this.url + '/runs/movie/' + runID);
  }
}