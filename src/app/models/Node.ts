export interface Node {
    id: Number;
    node_id: string;
    topology_id: number;
}