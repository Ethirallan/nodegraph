export interface Path {
    id: number;
    node_id: number;
    edge_id: number;
    message_id: number;
    timestamp: any;
}