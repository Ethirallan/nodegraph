import { Edge } from './Edge';
import { Node } from './Node';

export interface Frame {
    add_node: Node;
    add_edge: Edge;
    remove_node: Node;
    remove_edge: Edge;
}