export interface Message {
    id: number;
    origin: number;
    protocol: number;
    hops: number;
    topology_id: number;    
    timestamp: any;
}