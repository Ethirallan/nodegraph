export interface Edge {
    id: Number;
    node1_id: any;
    node2_id: any;
    topology_id: number;
}