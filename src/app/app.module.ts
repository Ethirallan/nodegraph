import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { GraphComponent } from './components/graph/graph.component';
import { CytographComponent } from './components/cytograph/cytograph.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MessageDetailsComponent } from './components/message-details/message-details.component';
import { NetworkMovieComponent } from './components/network-movie/network-movie.component';
import { MessageFlowComponent } from './components/message-flow/message-flow.component';
import { MenuComponent } from './components/menu/menu.component';

@NgModule({
  declarations: [
    AppComponent,
    GraphComponent,
    CytographComponent,
    NavbarComponent,
    MessageDetailsComponent,
    NetworkMovieComponent,
    MessageFlowComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
