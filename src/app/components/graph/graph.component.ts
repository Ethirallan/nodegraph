import { Component, OnInit } from '@angular/core';
import * as Sigma from 'sigma';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {

  private s: any;

  private graph = {
    nodes: [
      {id: 'n0', label: 'A node', x: 0, y: 0, size: 3, color: '#008cc2'},
      {id: 'n1', label: 'Another node', x: 3, y: 1, size: 2, color: '#008cc2'},
      {id: 'n2', label: 'And a last one', x: 1, y: 3, size: 1, color: '#E57821'}
    ],
    edges: [
      {id: 'e0', source: 'n0', target: 'n1', color: '#282c34', type: 'line', size: 0.5},
      {id: 'e1', source: 'n1', target: 'n2', color: '#282c34', type: 'curve', size: 1},
      {id: 'e2', source: 'n2', target: 'n0', color: '#FF0000', type: 'line', size: 2}
    ]
  };

  constructor() {

  }

  ngOnInit() {
    this.s = new Sigma(
      {
        renderer: {
          container: document.getElementById('sigma-container'),
          type: 'canvas'
        },
        settings: {
          animationsTime: 1000
        }
      }
    );
    this.s.graph.read(this.graph);
    this.s.refresh();

    Sigma.sigma.plugins.animate(
      this.s,
      {
        x: '2',
        y: '2',
        size: '5',
        color: '#000000'
      },
      {
        nodes: ['n0', 'n1', 'n2'],
        easing: 'cubicInOut',
        duration: 300,
      }
    );
  }
}
