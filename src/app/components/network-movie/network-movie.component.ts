import { Component, OnInit, AfterContentInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Edge } from 'src/app/models/Edge';
import { Path } from 'src/app/models/Path';
import { Node } from 'src/app/models/Node';
import { Frame } from 'src/app/models/Frame';

declare var cytoscape: any;

@Component({
  selector: 'app-network-movie',
  templateUrl: './network-movie.component.html',
  styleUrls: ['./network-movie.component.css']
})
export class NetworkMovieComponent implements OnInit {

  @Input('movie')
  public movie: Frame[];

  currentTopology: number = 0;

  cy: any;
  animationVersion: number = 0;
  movieVersion: number = 0;

  constructor() { }

  ngOnInit() {
    
  }

  ngAfterViewInit(): void {
    this.initCytoscape();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.playMovie();
  }

  playMovie() {
    this.currentTopology = 0;
    this.movieVersion++;
    var version = this.movieVersion;

    // for (let i = 0; i < this.movie.length; i++) {
    for (let i = 0; i < 9; i++) {
      setTimeout(() => {
        if (version == this.movieVersion) {
          var frame = this.movie[i];
          this.currentTopology++;
          console.log('Frame ' + i);
          if (frame.add_node != null) {
            console.log('Add Node ' + frame.add_node.id);
            this.addNode(frame.add_node.id);
          } else if (frame.remove_node != null) {
            console.log('Remove Node');
          } else if (frame.add_edge != null) {
            console.log('Add Edge ' + frame.add_edge[0].node1_id + '-' + frame.add_edge[0].node2_id);
            this.addEdge(parseInt(frame.add_edge[0].node1_id), parseInt(frame.add_edge[0].node2_id));
            this.addEdge(parseInt(frame.add_edge[1].node1_id), parseInt(frame.add_edge[1].node2_id));
          } else if (frame.remove_edge != null) {
            console.log('Remove Edge');
          }
          this.buildLayout();
        }
      }, 1000 * i, 1000);
    }
    this.buildLayout();
  }

  stopMovie() {
    this.movieVersion++;
  }

  replayMovie() {
    this.cy.elements().remove()
    this.playMovie();
  }

  buildLayout() {
    this.cy.layout({
      name: 'circle',
      directed: true,
      roots: '#' + 15,
      padding: 10
    }).run();
  }

  addNode(nodeID: Number) {
    this.cy.add({
      group: 'nodes',
      data: { id: nodeID, weight:  Math.floor(Math.random() * 10), }
    });
  }

  addEdge(n1: Number, n2: Number) {
    this.cy.add([
      { group: 'edges', data: { id: n1 + '-' + n2, source: n1, target: n2 } }
    ]);

    // this.cy.layout({
    //   name: 'circle',
    //   directed: true,
    //   roots: '#' + 15,
    //   padding: 10
    // }).run();
  }

  initCytoscape() {
    this.cy = cytoscape({
      container: document.getElementById('cy2'),
  
      boxSelectionEnabled: false,
      autounselectify: true,
  
      style: cytoscape.stylesheet()
        .selector('node')
        .style({
          'content': 'data(id)'
        })
        .selector('edge')
        .style({
          'curve-style': 'bezier',
          'target-arrow-shape': 'triangle',
          'width': 4,
          'line-color': '#ddd',
          'target-arrow-color': '#ddd'
        })
        .selector('.highlighted')
        .style({
          'background-color': '#61bffc',
          'line-color': '#61bffc',
          'target-arrow-color': '#61bffc',
          'transition-property': 'background-color, line-color, target-arrow-color',
          'transition-duration': '0.5s'
        })
        .selector('.origin-node')
        .style({
          'background-color': '#FF0000',
          'line-color': '#61bffc',
          'target-arrow-color': '#61bffc',
          'transition-property': 'background-color, line-color, target-arrow-color',
          'transition-duration': '0.5s'
        })
        .selector('.path')
        .style({
          'background-color': '#61bffc',
          'line-color': '#61bffc',
          'target-arrow-color': '#61bffc',
          'transition-property': 'background-color, line-color, target-arrow-color',
          'transition-duration': '0.5s'
        }).
        selector('.receiver')
        .style({
          'background-color': '#00ff00',
          'line-color': '#61bffc',
          'target-arrow-color': '#61bffc',
          'transition-property': 'background-color, line-color, target-arrow-color',
          'transition-duration': '0.5s'
        }),
    });
  }
}