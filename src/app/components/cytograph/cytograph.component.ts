import { Component, OnInit, AfterContentInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Edge } from 'src/app/models/Edge';
import { Path } from 'src/app/models/Path';
import { Node } from 'src/app/models/Node';

declare var cytoscape: any;

@Component({
  selector: 'app-cytograph',
  templateUrl: './cytograph.component.html',
  styleUrls: ['./cytograph.component.css']
})
export class CytographComponent implements OnInit {

  @Input('messageData')
  public messageData: {
    nodes: Node[],
    edges: Edge[],
    origin: Node,
    path: Path[]
  };

  cy: any;
  animationVersion: number = 0;

  constructor() { }

  ngOnInit() {
    this.initCytoscape();
    this.drawGraph();
    this.animateMessage();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.rebuildGraph();
    this.animateMessage();
  }

  drawGraph() {
    this.messageData.nodes.forEach(node => {
      this.addNode(node.id);
    });

    this.messageData.edges.forEach(edge => {
      this.addEdge(edge.node1_id, edge.node2_id);
    });

    this.cy.layout({
      name: 'circle',
      directed: true,
      roots: '#' + this.messageData.origin.id,
      padding: 10
    }).run();
  }

  animateMessage() {
    this.animationVersion++;
    var version = this.animationVersion;
    this.paintElement(this.messageData.origin.id, 'origin-node');

    for (let i = 0; i < this.messageData.path.length; i++) {
      setTimeout(() => {
        if (version == this.animationVersion) {
          this.paintElement(this.messageData.path[i].edge_id, 'path');
        }
      }, 2000 * i, 1100);

      setTimeout(() => {
        if (version == this.animationVersion) {
          this.paintElement(this.messageData.path[i].node_id, 'receiver')
        }
      }, 2000 * i, 2100);
    }
  }

  addNode(nodeID: Number) {
    this.cy.add({
      group: 'nodes',
      data: { id: nodeID, weight:  Math.floor(Math.random() * 10), }
    });
  }

  addEdge(n1: Number, n2: Number) {
    this.cy.add([
      { group: 'edges', data: { id: n1 + '-' + n2, source: n1, target: n2 } }
    ]);
  }

  paintElement(element: Number, style: string) {
    this.cy.elements().getElementById(element).addClass(style);
  }

  rebuildGraph() {
    if (this.cy != undefined) {
          this.cy.destroy();
    }
    this.initCytoscape();
    this.drawGraph();
  }

  replayAnimation() {
    this.rebuildGraph();
    this.animateMessage();
  }

  stopAnimation() {
    this.animationVersion++;
    this.rebuildGraph();
  }

  initCytoscape() {
    this.cy = cytoscape({
      container: document.getElementById('cy'),
  
      boxSelectionEnabled: false,
      autounselectify: true,
  
      style: cytoscape.stylesheet()
        .selector('node')
        .style({
          'content': 'data(id)'
        })
        .selector('edge')
        .style({
          'curve-style': 'bezier',
          'target-arrow-shape': 'triangle',
          'width': 4,
          'line-color': '#ddd',
          'target-arrow-color': '#ddd'
        })
        .selector('.highlighted')
        .style({
          'background-color': '#61bffc',
          'line-color': '#61bffc',
          'target-arrow-color': '#61bffc',
          'transition-property': 'background-color, line-color, target-arrow-color',
          'transition-duration': '0.5s'
        })
        .selector('.origin-node')
        .style({
          'background-color': '#FF0000',
          'line-color': '#61bffc',
          'target-arrow-color': '#61bffc',
          'transition-property': 'background-color, line-color, target-arrow-color',
          'transition-duration': '0.5s'
        })
        .selector('.path')
        .style({
          'background-color': '#61bffc',
          'line-color': '#61bffc',
          'target-arrow-color': '#61bffc',
          'transition-property': 'background-color, line-color, target-arrow-color',
          'transition-duration': '0.5s'
        }).
        selector('.receiver')
        .style({
          'background-color': '#00ff00',
          'line-color': '#61bffc',
          'target-arrow-color': '#61bffc',
          'transition-property': 'background-color, line-color, target-arrow-color',
          'transition-duration': '0.5s'
        }),
    });
  }
}