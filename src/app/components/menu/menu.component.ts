import { Component, OnInit } from '@angular/core';
import { DbService } from 'src/app/services/db.service';
import { Run } from 'src/app/models/Run';
import { Topology } from 'src/app/models/Topology';
import { Edge } from 'src/app/models/Edge';
import { Path } from 'src/app/models/Path';
import { Message } from 'src/app/models/Message';
import { Node } from 'src/app/models/Node';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  runs: Run[] = [];
  topologies: Topology[] = [];
  nodes: Node[] = [];
  edges: Edge[] = [];
  messages: Message[] = [];
  message: Message;
  paths: Path[] = [];

  activeRun: number;
  activeTopology: number;
  activeMessage: number;

  node: Node;

  movie: any = [];

  constructor(public dbService: DbService) { }

  ngOnInit() {
    this.getRuns();
    this.getMovieFrames(1);
  }

  getRuns() {
    this.dbService.getAllRuns().subscribe(res => {
      console.log(res);
      this.runs = res['message'];
    }, error => {
      console.log(error);
    });
  }

  getTopologies(runID: number, index: number) {
    this.activeRun = index;
    this.dbService.getAllTopologies(runID).subscribe(res => {
      console.log(res);
      this.topologies = res['message'];
    }, error => {
      console.log(error);
    });
  }

  getNodes(topologyID: number) {
    this.dbService.getAllNodes(topologyID).subscribe(res => {
      console.log(res);
      this.nodes = res['message'];
    }, error => {
      console.log(error);
    });
  }

  getEdges(topologyID: number) {
    this.dbService.getAllEdges(topologyID).subscribe(res => {
      console.log(res);
      this.edges = res['message'];
    }, error => {
      console.log(error);
    });
  }

  getMessages(topologyID: number, index: number) {
    this.activeTopology = index;
    this.dbService.getAllMessages(topologyID).subscribe(res => {
      console.log(res);
      this.messages = res['message'];
    }, error => {
      console.log(error);
    });
  }

  getMessageDetails(messageID: number, index: number) {
    this.activeMessage = index;
    this.dbService.getMessageByID(messageID).subscribe(res => {
      this.message = res['message'];
      console.log(this.message);
    }, error => {
      console.log(error);
    });
  }
  
  getNodeByID(id: number) {
    this.dbService.getNodeByID(id).subscribe(res => {
      console.log(res);
    }, error => {
      console.log(error);
    });
  }

  getPaths(messageID: number) {
    this.dbService.getAllNodes(messageID).subscribe(res => {
      console.log(res);
      this.paths = res['message'];
    }, error => {
      console.log(error);
    });
  }

  getMovieFrames(runID: number) {
    this.dbService.getMovieByID(runID).subscribe(res => {
      this.movie = res['frames'];
    }, error => {
      console.log(error);
    });
  }
}

