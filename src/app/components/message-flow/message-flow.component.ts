import { Component, OnInit } from '@angular/core';
import { DbService } from 'src/app/services/db.service';

@Component({
  selector: 'app-message-flow',
  templateUrl: './message-flow.component.html',
  styleUrls: ['./message-flow.component.css']
})
export class MessageFlowComponent implements OnInit {

  public topologyID: number = 1;
  public runID: number = 1;

  // public topologies: Topology[] = [];
  public topologies: any[] = [];

  constructor(public dbService: DbService) { }

  ngOnInit() {
    this.getMessages();
    this.getTopologies();
  }

  getMessages() {
    this.dbService.getAllMessages(this.topologyID).subscribe(res => {
      console.log(res);
    }, error => {
      console.log(error);
    });
  }

  getTopologies() {
    this.dbService.getAllTopologies(this.runID).subscribe(res => {
      console.log(res);
      this.topologies = res['message'];
      console.log(this.topologies);
    }, error => {
      console.log(error);
    });
  }
}
